"""
Для таблицы «материала» из дополнительного задания создайте пользовательскую агрегатную функцию, которая считает
среднее значение весов всех материалов результирующей выборки и округляет данной значение до целого.
"""
import materials
import sqlite3


class RowSet:
    """
    Клас для реалізації власної агрегатної функції: Середнє
    """

    def __init__(self):
        # ініціалізація контейнера
        self.count = 0
        self.len = 0

    def step(self, value):
        # додавання елемента в контейнер
        self.count += value
        self.len += 1

    def finalize(self):
        # розрахунок агрегації
        if self.len == 0:
            return 0
        else:
            return round(self.count / self.len)


if __name__ == '__main__':
    conn = sqlite3.connect('data/db.sqlite3')
    conn.create_aggregate('row_set', 1, RowSet)

    materials.create_db(conn)
    materials.init_db(conn)

    """
    materials.print_table(conn, "materials")
    materials.print_table(conn, "add_characteristics")
    """
    cursor = conn.execute('SELECT row_set(weight) FROM materials')
    result = cursor.fetchone()[0]
    print(f'Result of aggregate function: {result}')

    conn.close()

