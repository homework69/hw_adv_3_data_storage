"""
Для таблицы «материала» из дополнительного задания создайте пользовательскую функцию, которая принимает неограниченное
количество полей и возвращает их конкатенацию.
"""

import sqlite3
import materials


def concat_fields(*args):
    str_args = [str(_) for _ in args]
    return ' && '.join(str_args)


if __name__ == '__main__':
    conn = sqlite3.connect('data/db.sqlite3')
    conn.create_function('concat_fields', -1, concat_fields)

    materials.create_db(conn)
    materials.init_db(conn)

    """
    materials.print_table(conn, "materials")
    materials.print_table(conn, "add_characteristics")
    """

    result = conn.execute('SELECT concat_fields(id, weight, hight) FROM materials')
    while True:
        current_ = result.fetchone()
        if current_ is None:
            break
        print(current_[0])

    conn.close()
