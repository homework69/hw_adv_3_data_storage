"""
Создайте таблицу «материалы» из следующих полей: идентификатор, вес, высота и доп.
характеристики материала. Поле доп. характеристики материала должно хранить в себе массив,
каждый элемент которого является кортежем из двух значений, первое – название
характеристики, а второе – её значение.

"""


def create_db(conn):
    cursor = conn.cursor()

    query_text = """
        CREATE TABLE if NOT EXISTS "materials"(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        weight REAL,
        hight REAL
        )"""
    cursor.execute(query_text)

    query_text = """
        CREATE TABLE if NOT EXISTS "add_characteristics"(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        char_name VARCHAR(30),
        char_value VARCHAR(50),
        id_material INTEGER
        )"""
    cursor.execute(query_text)


def init_db(conn):
    cursor = conn.cursor()

    cursor.execute('SELECT id FROM materials')

    if cursor.fetchone() is None:
        query_text = """
            INSERT INTO materials(weight, hight)
            VALUES (100.5, 2.),
                   (201.5, 1.5),
                   (35, 1)
            """
        cursor.execute(query_text)

    cursor.execute('SELECT id FROM add_characteristics')
    if cursor.fetchone() is None:
        query_text = """
            INSERT INTO add_characteristics(char_name, char_value, id_material)
            VALUES ("color", "gray", 1),
                   ("form", "piramid", 1),
                   ("color", "brown", 2),
                   ("form", "cone", 2)
            """
        cursor.execute(query_text)


def print_table(conn, table_name, count_records=0):
    print(f'Records from table {table_name}:')
    sql_text = 'SELECT' + \
               ('' if count_records == 0 else f' FIRST {str(count_records)}') + \
               f' * FROM {table_name}'
    result = conn.execute(sql_text, {'table': table_name})
    while True:
        current_ = result.fetchone()
        if current_ is None:
            break
        print(current_)
