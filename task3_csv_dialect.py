"""
Поработайте с созданием собственных диалектов, произвольно выбирая правила для CSV файлов.
Зарегистрируйте созданные диалекты и поработайте, используя их, с созданием/чтением файлом.
"""
import csv


class MyDialect(csv.Dialect):
    quoting = csv.QUOTE_MINIMAL
    quotechar = '!'
    delimiter = ';'
    lineterminator = '\n'
    doublequote = False
    escapechar = '\\'


csv.register_dialect('my_dialect', MyDialect)

with open('data/output.csv', 'w') as file:
    writer = csv.writer(file, dialect='my_dialect')
    writer.writerow(['1', '2', '3'])
    writer.writerow(['1', '2', '3'])
    writer.writerow(['1', '2', '3'])


with open('data/dict_output.csv', 'w') as file:
    fieldnames = ['name', 'category', 'quantity']
    writer = csv.DictWriter(file, fieldnames=fieldnames, dialect='my_dialect')
    writer.writeheader()
    writer.writerow({
        'name': 'S52',
        'category': 'phone',
        'quantity': '3'})
    writer.writerow({
        'name': 'Iphone! 11',
        'category': 'phone',
        'quantity': '2'})
    writer.writerow({
        'name': 'Asus VivoBook',
        'category': 'note-book',
        'quantity': '12'})

with open('data/output.csv', 'r') as file:
    reader = csv.reader(file, dialect='my_dialect')
    print('\nRead csv as Lists:')
    for row in reader:
        print(row)

with open('data/dict_output.csv', 'r') as file:
    reader = csv.DictReader(file, dialect='my_dialect')
    print('\nRead csv as Dicts:')
    for row in reader:
        print(row)
