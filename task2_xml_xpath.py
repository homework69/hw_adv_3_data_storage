"""
Создайте XML файл с вложенными элементами и воспользуйтесь языком поиска XPATH. Попробуйте осуществить поиск
содержимого по созданному документу XML, усложняя свои запросы и добавляя новые элементы, если потребуется.
"""

from faker import Faker
from xml.etree import ElementTree as ET

fake = Faker("uk_UA")
persons_data = ET.Element('records')
id_ = 0
for _ in range(10):
    record = ET.SubElement(persons_data, 'record', attrib={'id': str(id_)})
    current_data = {'name': fake.name(), 'phone': fake.phone_number(), 'email': fake.ascii_email()}
    for key, value in current_data.items():
        elem = ET.SubElement(record, key)
        elem.text = value
    id_ += 1

tree = ET.ElementTree(persons_data)
tree.write('data/persons.xml', encoding='utf-8')

root = tree.getroot()
names = root.findall('./record[@id="2"]/name')
emails = root.findall('./record[@id="2"]/email')
for elements in zip(names, emails):
    row = {element.tag: element.text for element in elements}
    print(row)

names = root.findall('./record[@id]/name')
emails = root.findall('./record[@id]/email')
for elements in zip(names, emails):
    row = {element.tag: element.text for element in elements}
    print(row)

