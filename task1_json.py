"""
Создайте простые словари и сконвертируйте их в JSON. Сохраните JSON в файл и попробуйте загрузить данные из файла.
"""

import datetime
import json
from typing import Any


class DateFormatEncoder(json.JSONEncoder):
    """
    Клас для конвертації дати та часу у формат json
    """

    def default(self, obj: Any) -> dict:
        if isinstance(obj, datetime.date):
            return {
                'value': obj.strftime('%Y-%m-%d'),
                '__date__': True
            }
        elif isinstance(obj, datetime.datetime):
            return {
                'value': obj.strftime('%Y-%m-%d %H:%M:%S'),
                '__datetime__': True
            }
        elif isinstance(obj, datetime.time):
            return {
                'value': obj.strftime('%H:%M:%S'),
                '__time__': True
            }
        else:
            return json.JSONEncoder.default(self, obj)


def hook_date_datetime_time(dct: dict) -> datetime.datetime | datetime.date | datetime.time:
    """
    Функція оберненої конвертації
    :param dct: dict
    :return: json str
    """
    if '__date__' in dct:
        return datetime.datetime.strptime(dct['value'], '%Y-%m-%d').date()
    elif '__datetime__' in dct:
        return datetime.datetime.strptime(dct['value'], '%Y-%m-%d %H:%M:%S')
    elif '__time__' in dct:
        return datetime.datetime.strptime(dct['value'], '%H:%M:%S').time()
    else:
        return dct


if __name__ == '__main__':
    data = [
        {
            'name': 'Ivan',
            'birthday': datetime.datetime(1983, 4, 13),
            'start time': datetime.time(9, 30),
            'skills': [
                'python',
                'SQL',
                'GIT'
            ]
        },
        {
            'name': 'Petro',
            'birthday': datetime.datetime(1984, 7, 31),
            'start time': datetime.time(10, 00),
            'skills': [
                'PHP',
                'Rest',
                'java script'
            ]
        }
    ]

    """
    json_data = json.dumps(data, cls=DateFormatEncoder, indent=4)
    print(json_data)
    """

    with open('data/output.json', 'w') as f:
        json.dump(data, f, cls=DateFormatEncoder, indent=4)

    with open('data/output.json', 'r') as f_read:
        data = json.load(f_read, object_hook=hook_date_datetime_time)
        for person in data:
            print(person)
